import { BrowserRouter, Route, Routes } from "react-router-dom";
import PageSplash from "./ui/PageSplash";
import PageMain from "./ui/PageMain";
import { GlobalStyle } from "./AppStyle";

function App() {
  return (
    <>
     <GlobalStyle />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageSplash />} />
          <Route path="/form" element={<PageMain />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
