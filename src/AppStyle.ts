import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding:0;
    }

    p, a, h1, th, td, tr, label, input, option, button {
        margin: 0;
        padding: 0;
        font-family: 'Roboto Condensed', sans-serif;
    }

    *,
    *::before,
    *::after {
      box-sizing: border-box;
    }
`;
