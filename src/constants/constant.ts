
export const COLOR = {
   color_white: '#ffffff',
   color_gray: '#C8C8C8',
   color_gray_dark: '#656F7D',
   color_black: '#000000',
   color_red: '#ff0000',
   color_red_light: '#ffabab',
   color_gray_light: '#EFEFEF',
   color_gray_light_blue: '#eceef4',
   color_gray_xlight_blue: '#f0f2f7',
   color_light_blue: '#D2DAE5',
   color_background: '#f5f5f5',
   color_green: '#00bd55',
   color_background_main: '#f2faff',
   color_background_splash: '#4c4482',
   color_footer_bg: '#dde7ed',
   color_app_bar: '#4DB6AC',
   color_search_active: '#00897B',
   color_white_alpha: 'rgba(255, 255, 255, 0.5)',
   color_app_bar_count: '#004D40',
   color_content_bg: '#f5f5f5',
   color_material_green: '#1DE9B6',
   color_material_green_dark: '#00BFA5',
   color_android_green: '#3DDC84',
   color_android_green_light: '#ccffe3',
   color_android_green_dark: '#28BA69',
   color_blue_dark: '#073042',
   color_blue: '#246b8a'
}

export const TEXT_SIZE = {
   size_small: '0.5rem',
   size_medium: '1.0rem',
   size_normal: '1.2rem',
   size_normal_mobile: '0.8rem',
   size_large: '1.5rem',
   size_xlarge: '2.5rem',
   size_xlarge_tablet: '2rem'
}

export const FORM = {
   OPTION_INSURANCE_SHORT: 'OPTION_INSURANCE_SHORT',
   OPTION_INSURANCE_YEAR: 'OPTION_INSURANCE_YEAR',
   OPTION_PACKAGE_BASE: 'OPTION_PACKAGE_BASE',
   OPTION_PACKAGE_EXTENDED: 'OPTION_PACKAGE_EXTENDED',
   OPTION_PACKAGE_EXTRA: 'OPTION_PACKAGE_EXTRA'
}

/**********************************************************************
 *  kratkodobe poistenie ( 1.2 eur/den, 1.8 eur/den, 2.4 eur/den )
 *  celorocne poistenie (39eur, 49eur, 59eur ) 
 *  extra tarif ( 10, 20, 30, 50 ) 
 **********************************************************************/
export const TARIF = {
   SHORT: {
     T_12: 1.2, // 1.2,- eur/den
     T_18: 1.8,
     T_24: 2.4,
   },
 
   YEAR: {
     T_39: 39, // 39,- eur/rok
     T_49: 49,
     T_59: 59,
   },
 
   EXTRA: {
     T_10: 1.1, // 10%
     T_20: 1.2,
     T_30: 1.3,
     T_50: 1.5,
   },
 }




