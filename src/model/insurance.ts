
export interface Insurance {
  insuranceOption?: InsuranceOptionEnum;
  insuranceDateStart?: string | null;
  insuranceDateEnd?: string | null;
  insurancePackage?: InsurancePackageEnum;
  additionalInsurance?: InsuranceAdditional;
  bodyCount?: number;
}

export enum InsuranceOptionEnum {
  INSURANCE_DEFAULT = 'INSURANCE_DEFAULT',
  INSURANCE_SHORT = 'OPTION_INSURANCE_SHORT',
  INSURANCE_YEAR = 'OPTION_INSURANCE_YEAR'
}

export enum InsurancePackageEnum {
  PACKAGE_DEFAULT = 'PACKAGE_DEFAULT',
  PACKAGE_BASE = 'OPTION_PACKAGE_BASE',
  PACKAGE_EXTENDED = 'OPTION_PACKAGE_EXTENDED',
  PACKAGE_EXTRA = "OPTION_PACKAGE_EXTRA" 
}

export interface InsuranceAdditional {
  isCancelTtrip: boolean,
  isSportActivity: boolean
}

