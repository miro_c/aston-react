import {Dispatch} from 'react';
import {ActionEnum, StateEnum} from './reducer';
import { Insurance } from "../model/insurance";

export const dispatchAction = async (
  dispatch: Dispatch<any>,
  action: ActionEnum
) => {
  dispatch({
    type: action,
    payload: {
      state: StateEnum.CALCULATING
    },
  });
};

export const dispatchActionDateStart = async (
  dispatch: Dispatch<any>,
  action: ActionEnum,
  date: string
) => {
  dispatch({
    type: action,
    payload: {
      state: StateEnum.CALCULATING,
      data:{
        insuranceDateStart: date
      }
    },
  });
};

export const dispatchActionDateEnd = async (
  dispatch: Dispatch<any>,
  action: ActionEnum,
  date: string
) => {
  dispatch({
    type: action,
    payload: {
      state: StateEnum.CALCULATING,
      data:{
        insuranceDateEnd: date
      }
    },
  });
};

export const dispatchActionCancelTrip = async (
  dispatch: Dispatch<any>,
  action: ActionEnum,
  isActive: boolean
) => {
  dispatch({
    type: action,
    payload: {
      state: StateEnum.CALCULATING,
      data: {
        additionalInsurance: {
          isCancelTtrip: isActive,
        },
      },
    },
  });
};

export const dispatchActionSportActivity = async (
  dispatch: Dispatch<any>,
  action: ActionEnum,
  isActive: boolean
) => {
  dispatch({
    type: action,
    payload: {
      state: StateEnum.CALCULATING,
      data: {
        additionalInsurance: {
          isSportActivity: isActive,
        },
      },
    },
  });
};

export const dispatchActionBodyCount = async (
  dispatch: Dispatch<any>,
  action: ActionEnum,
  count: number
) => {
  dispatch({
    type: action,
    payload: {
      state: StateEnum.CALCULATING,
      data: {
        bodyCount: count,
      },
    },
  });
};

export const dispatchInsuranceFinal = async (
  dispatch: Dispatch<any>,
  data: Insurance,
) => {
  dispatch({
    type: ActionEnum.INSURANCE_FINAL,
    payload: {
      state: StateEnum.CALCULATE_FINAL,
      data: data,
    },
  });
};

export const dispatchInsuranceClearValue = async (
  dispatch: Dispatch<any>
) => {
  dispatch({
    type: ActionEnum.CLEAR_INSURANCE_VALUE,
    payload: {
      state: StateEnum.CALCULATE_FINAL,
      data: null,
    },
  });
};

