import { TARIF } from "../constants/constant";
import { Insurance, InsuranceOptionEnum, InsurancePackageEnum } from "../model/insurance";

export enum StateEnum {
  CALCULATING = 'CALCULATING',
  CALCULATE_FINAL = 'CALCULATE_FINAL'
}

export enum ActionEnum {
  OPTION_INSURANCE_SHORT = "OPTION_INSURANCE_SHORT",
  OPTION_INSURANCE_YEAR = "OPTION_INSURANCE_YEAR",
  SELECT_DATE_START = "SELECT_DATE_START",
  SELECT_DATE_END = "SELECT_DATE_END",
  OPTION_PACKAGE_BASE = "OPTION_PACKAGE_BASE",
  OPTION_PACKAGE_EXTENDED = "OPTION_PACKAGE_EXTENDED",
  OPTION_PACKAGE_EXTRA = "OPTION_PACKAGE_EXTRA",
  ADDITIONAL_INSURANCE_CANCEL = "ADDITIONAL_INSURANCE_CANCEL",
  ADDITIONAL_INSURANCE_ACTIVITY = "ADDITIONAL_INSURANCE_ACTIVITY",
  BODY_COUNT = "BODY_COUNT",
  INSURANCE_FINAL = "INSURANCE_FINAL",
  CLEAR_INSURANCE_VALUE = "CLEAR_INSURANCE_VALUE"
}

export interface ReducerAction {
  type: ActionEnum;
  payload: CalculatorState;
}

export interface CalculatorState {
  state: StateEnum;
  data?: Insurance;
}

const insuranceDefault = {
  insuranceOption: InsuranceOptionEnum.INSURANCE_DEFAULT,
  insuranceDateStart: null,
  insuranceDateEnd: null,
  insurancePackage: InsurancePackageEnum.PACKAGE_DEFAULT,
  additionalInsurance: {
    isCancelTtrip: false,
    isSportActivity: false
  },
  bodyCount: 1
} as Insurance

const defaultAppState: CalculatorState = {
  state: StateEnum.CALCULATING,
  data: insuranceDefault,
} as CalculatorState;

export const CalculatorReducer = (
  state: CalculatorState = defaultAppState,
  action: ReducerAction
): CalculatorState | null => {
  switch (action.type) {
    case ActionEnum.OPTION_INSURANCE_SHORT:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          insuranceOption: InsuranceOptionEnum.INSURANCE_SHORT,
        },
      };

    case ActionEnum.OPTION_INSURANCE_YEAR:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          insuranceOption: InsuranceOptionEnum.INSURANCE_YEAR,
        },
      };

    case ActionEnum.SELECT_DATE_START:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          insuranceDateStart: action.payload?.data?.insuranceDateStart,
        },
      };

    case ActionEnum.SELECT_DATE_END:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          insuranceDateEnd: action.payload?.data?.insuranceDateEnd,
        },
      };

    case ActionEnum.OPTION_PACKAGE_BASE:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          insurancePackage: InsurancePackageEnum.PACKAGE_BASE,
        },
      };

    case ActionEnum.OPTION_PACKAGE_EXTENDED:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          insurancePackage: InsurancePackageEnum.PACKAGE_EXTENDED,
        },
      };

    case ActionEnum.OPTION_PACKAGE_EXTRA:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          insurancePackage: InsurancePackageEnum.PACKAGE_EXTRA,
        },
      };

    case ActionEnum.ADDITIONAL_INSURANCE_CANCEL:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          additionalInsurance: {
            isCancelTtrip: action.payload?.data?.additionalInsurance?.isCancelTtrip!,
            isSportActivity: state?.data?.additionalInsurance?.isSportActivity!,
          },
        },
      };

    case ActionEnum.ADDITIONAL_INSURANCE_ACTIVITY:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          additionalInsurance: {
            isCancelTtrip: state?.data?.additionalInsurance?.isCancelTtrip!,
            isSportActivity: action.payload?.data?.additionalInsurance?.isSportActivity!,
          },
        },
      };

    case ActionEnum.BODY_COUNT:
      return {
        state: action.payload?.state,
        data: {
          ...state?.data,
          bodyCount: action.payload?.data?.bodyCount,
        },
      };

    case ActionEnum.INSURANCE_FINAL:
      return {
        state: action.payload?.state,
        data: action.payload?.data,
        // TODO
      };

    case ActionEnum.CLEAR_INSURANCE_VALUE:
      return {
        state: action.payload?.state,
        data: action.payload?.data,
      };

    default:
      return state;
  }
};
