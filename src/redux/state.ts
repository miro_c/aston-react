import { combineReducers } from 'redux';
import { CalculatorReducer } from "./reducer";

export const rootReducer = combineReducers({
  calculatorState: CalculatorReducer
});

export type State = ReturnType<typeof rootReducer>;