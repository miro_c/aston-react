import styled, {createGlobalStyle} from 'styled-components';
import { COLOR, TEXT_SIZE } from '../constants/constant';

export const CheckboxLabel = styled.label`
  display: block;
  position: relative;
  padding-left: 2em;
  padding-top: 0.2em;
  padding-right: 0.7em;
  padding-bottom: 0.2em;
  cursor: pointer;
  font-size: ${TEXT_SIZE.size_medium};
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  text-align: center;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  input {
    position: absolute;
    opacity: 1;
    cursor: pointer;
    height: 0;
    width: 0;
  }
  .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 1.5em;
    width: 1.5em;
    background-color: ${COLOR.color_white};
    border: 0.15em solid ${COLOR.color_android_green};
    -webkit-border-radius: 0.35em;
    -moz-border-radius: 0.35em;
    border-radius: 0.35em;
  }
  &:hover input ~ .checkmark {
    background-color: ${COLOR.color_android_green_light};
  }
  input:checked ~ .checkmark {
    background-color: ${COLOR.color_android_green};
    border: 0.1em solid ${COLOR.color_android_green_dark};
  }
  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }
  input:checked ~ .checkmark:after {
    display: block;
  }
  .checkmark:after {
    left: 0.45em;
    top: 0.2em;
    width: 0.4em;
    height: 0.8em;
    border: solid white;
    border-width: 0 0.2em 0.2em 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`;

export const CheckboxTitle = styled.p`
  color: ${COLOR.color_gray_dark};
  font-size: ${TEXT_SIZE.size_medium};
  font-style: bold;
`;

/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const CheckboxResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
     ${CheckboxLabel}{
    
     }

     ${CheckboxTitle}{
      font-size: ${TEXT_SIZE.size_normal_mobile};
     }
  
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
  
  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1920px) {
  
  }
`;
