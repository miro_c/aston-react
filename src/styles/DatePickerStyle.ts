import styled, {createGlobalStyle} from 'styled-components';
import { COLOR, TEXT_SIZE } from '../constants/constant';

export const PickerWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 7em;
  height: auto;
  background: ${COLOR.color_android_green};
  color: ${COLOR.color_gray};
  cursor: pointer;
  border: 0.1em solid ${COLOR.color_android_green};
  box-sizing: border-box;
  padding: 0.2em;
  -webkit-border-radius: 0.35em;
  -moz-border-radius: 0.35em;
  border-radius: 0.35em;
  z-index: 10;
  overflow: hiden;

  input {
    border: 0;
    outline: 0;
  }

  input:focus {
    outline: none !important;
  }

  input[type="date"]::-webkit-calendar-picker-indicator {
    width: 7em;
    height: 2em;
    margin: 0;
    color: rgba(0, 0, 0, 0);
    background: rgba(0, 0, 0, 0);
    cursor: pointer;
  }
`;

export const PickerInput = styled.input`
  position: absolute;
  width: 10em;
  height: auto;
  text-align: center;
  align-items: center;
  justify-content: center;
  border: none;
  opacity: 0;
  margin: -0.5em -2em -0.5em -2em;
  z-index: 999;
`;

export const CalendarText = styled.p`
  z-index: 10;
  margin: auto;
  padding: 0;
  text-align: center;
  align-items: center;
  justify-content: center;
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_normal_mobile};
  font-family: "Roboto", sans-serif;
  font-weight: 400;
`;

export const CalendarIcon = styled.img`
  margin-left: 0.1em;
  padding: 0.1em;
  z-index: 10;
`;

/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const DatePickerResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
     ${PickerWrapper}{
      width: 7em;
      height: auto;
      padding: 0.2em;

      input[type="date"]::-webkit-calendar-picker-indicator {
        width: 7em;
        height: 1.5em;
        margin: 0;
        color: rgba(0, 0, 0, 0);
        background: rgba(0, 0, 0, 0);
        cursor: pointer;
      }
     }
  
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
    ${PickerWrapper}{
  
     }
 
  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1920px) {
  
  }
`;
