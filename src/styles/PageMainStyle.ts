import styled, { keyframes, createGlobalStyle } from "styled-components";
import { COLOR, TEXT_SIZE } from "../constants/constant";

export const MainGrid = styled.div`
  width: 100vw;
  display: grid;
  grid-row-gap: 0em;
  grid-column-gap: 0em;
  grid-template-areas:
    "header header header header header"
    "content content content content content"
    "footer footer footer footer footer";
`;

export const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  grid-area: header;
  min-height: 5vh;
  background-color: ${COLOR.color_gray_light_blue};
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  grid-area: content;
  height: 90vh;
  background-color: ${COLOR.color_gray_light_blue};
  padding: 0.5em;
`;

const ScaleInAnimation = keyframes`
  0% {
      opacity: 0;
      transform: scale(0.9);
  }
  100% {
      opacity: 1;
      transform: scale(1.0);
   }
`;

export const Form = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 40vw;
  height: auto;
  column-gap: 2em;
  padding: 2.5em;
  overflow: auto;
  animation: ${ScaleInAnimation} ease 0.15s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
  background-color: ${COLOR.color_white};
  -webkit-box-shadow: 0px 0px 10px 6px rgba(0, 0, 0, 0.05);
  box-shadow: 0px 0px 10px 6px rgba(0, 0, 0, 0.05);
  -webkit-border-radius: 1.2em;
  -moz-border-radius: 1.2em;
  border-radius: 1.2em;
  border: 0.2em solid ${COLOR.color_blue_dark};
`;

export const Title = styled.h1`
  font-size: ${TEXT_SIZE.size_xlarge};
  font-family: "Roboto", sans-serif;
  font-weight: 900;
  text-align: center;
  margin-bottom:0.8em;
  color: ${COLOR.color_blue_dark};
`;

type ItemWrapperProps = {
  isCenter: boolean;
};
export const ItemWrapper = styled.div<ItemWrapperProps>`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: ${props => props.isCenter? 'center' : 'left'};
  width: 100%;
  height: auto;
  padding: 0.7em 0.7em 0.7em 0.7em;
  margin-top: 0em;
`;

export const ErrorTitle = styled.p`
  position: absolute ;
  font-size: ${TEXT_SIZE.size_normal_mobile};
  font-family: "Roboto", sans-serif;
  font-weight: 600;
  text-align: center;
  border-bottom: 0.15em solid ${COLOR.color_red_light};
  border-top: 0.15em solid ${COLOR.color_white};
  background: ${COLOR.color_white};
  box-sizing: border-box;
  -webkit-border-radius: 0.25em;
  -moz-border-radius: 0.25em;
  border-radius: 0.25em;
  color: ${COLOR.color_red};
  width: 15vw;
  padding: 0.2em;
  margin-top: 4.5em;
  margin-left: auto;
   margin-right: auto;
   left: 0;
   right: 0;
`;

type CalcValueProps = {
  isActive: boolean;
};
export const CalcValue = styled.p<CalcValueProps>`
  margin-left: auto;
  padding: 0em 0em 0em 0.5em;
  font-size: ${TEXT_SIZE.size_large};
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  text-align: center;
  color: ${props => props.isActive? COLOR.color_android_green : COLOR.color_light_blue};
`;

export const InfoWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: right;
  margin-left: auto;
  padding: 0em 0em 0em 0.5em;
  width:auto;
  height: auto;
`;
export const InfoIcon = styled.img`
  width:1.8em;
  height: auto ;
  margin: 0em 0.5em 0em 0.5em;
`;
type FaceProps = {
  isActive: boolean;
};
export const InfoText = styled.p<FaceProps>`
  padding: 0em 0em 0em 0.5em;
  font-size: ${TEXT_SIZE.size_normal};
  font-family: "Roboto", sans-serif;
  font-weight: 700;
  text-align: center;
  color: ${props => props.isActive? COLOR.color_blue_dark : COLOR.color_gray};
`;

export const InfoTextTitle = styled.p`
  font-size: ${TEXT_SIZE.size_normal};
  font-family: "Roboto", sans-serif;
  font-weight: 700;
  text-align: center;
  color: ${COLOR.color_blue_dark};
`;

export const ContinuousPriceWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: right;
  width: 100%;
  height: auto;
  column-gap: 0.5em;
  padding-right: 0.45em;
  box-sizing: border-box;
`;

export const ContinuousText = styled.p`
  border-top: 0.1em solid ${COLOR.color_android_green};
  font-size: ${TEXT_SIZE.size_medium};
  font-family: "Roboto", sans-serif;
  font-weight: 700;
  text-align: center;
  color: ${COLOR.color_android_green};
  padding-top: 0.25em;
`;

export const FinalPriceWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 50%;
  height: auto;
  column-gap: 0.5em;
  padding: 0.8em;
  margin-top: 0.3em;
  border: 0.2em solid ${COLOR.color_android_green};
  box-sizing: border-box;
  -webkit-border-radius: 0.75em;
  -moz-border-radius: 0.75em;
  border-radius: 0.75em;
`;

export const FinalPriceText = styled.p`
  max-height: 2em;
  font-size: ${TEXT_SIZE.size_xlarge};
  font-family: "Roboto", sans-serif;
  font-weight: bold;
  text-align: center;
  color: ${COLOR.color_black};
`;

export const FinalPriceTitle = styled.p`
  font-size: ${TEXT_SIZE.size_xlarge};
  font-family: "Roboto", sans-serif;
  font-weight: 900;
  text-align: center;
  color: ${COLOR.color_android_green};
`;

type DateTextProps = {
  isActive: boolean;
};
export const DateText = styled.p<DateTextProps>`
  font-size: ${TEXT_SIZE.size_large};
  font-family: "Roboto", sans-serif;
  font-weight: 900;
  text-align: center;
  color: ${props => props.isActive? COLOR.color_blue_dark : COLOR.color_light_blue};
  padding: 0.5em;
`;
export const DateTextSpan = styled.p<DateTextProps>`
  font-size: ${TEXT_SIZE.size_large};
  font-weight: 900;
  text-align: center;
  color: ${props => props.isActive? COLOR.color_blue_dark : COLOR.color_light_blue};
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 95%;
  height: auto;
  row-gap: 2em;
  padding: 1.5em 1.5em 1.5em 1.5em;
  margin-top: 0.5em;
`;

export const ButtonSubmit = styled.button`
  width: 100%;
  height: auto;
  background: ${COLOR.color_android_green};
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_normal};
  font-style: bold;
  border: 0.1em solid ${COLOR.color_android_green};
  -webkit-border-radius: 0.75em;
  -moz-border-radius: 0.75em;
  border-radius: 0.75em;
  cursor: pointer;
  padding: 1em;

  transition: background-color 0.15s ease-out 10ms;
  &:hover {
    background-color: ${COLOR.color_android_green_dark};
  }
`;

export const Label = styled.p`
  font-size: ${TEXT_SIZE.size_normal};
  font-family: "Roboto", sans-serif;
  font-weight: 900;
  text-align: center;
  color: ${COLOR.color_blue_dark};
  padding: 0em 0.5em 0em 0em;
`;

export const Footer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  grid-area: footer;
  height: 5vh;
  background-color: ${COLOR.color_gray_light_blue};
  padding: 0.5em;
`;

export const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: auto;
  position: relative;
  box-sizing: border-box;
  padding: 0.5em;
  -webkit-border-radius: 0.5em;
  -moz-border-radius: 0.5em;
  border-radius: 0.5em;
`;

export const ItemTitle = styled.p`
  color: ${COLOR.color_gray_dark};
  font-size: ${TEXT_SIZE.size_medium};
  font-style: bold;
`;

export const RadioButtonLabel = styled.label`
  position: absolute;
  top: 25%;
  left: 0.15em;
  width: 1.5em;
  height: 1.5em;
  border-radius: 50%;
  background: white;
  border: 0.15em solid ${COLOR.color_android_green};
`;

export const RadioButton = styled.input`
  opacity: 0;
  z-index: 1;
  cursor: pointer;
  width: 2em;
  height: 2em;
  &:hover ~ ${RadioButtonLabel} {
    background:  ${COLOR.color_android_green_light};
    &::after {
      display: block;
      color: white;
      width: 1.2em;
      height: 1.2em;
    }
  }
  &:checked + ${Item} {
    background: ${COLOR.color_android_green};
  }
  &:checked + ${RadioButtonLabel} {
    background: ${COLOR.color_android_green};;
    border: 0.1em solid ${COLOR.color_android_green_dark};
    &::after {
      display: block;
      color: white;
      width: 1.2em;
      height: 1.2em;
    }
  }
`;

/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const AppResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
     ${Form}{
      width: 100vw;
      column-gap: 0.1em;
      padding: 0.5em;
     }
     ${Header}{
      min-height: 5vh;
     }
     ${Content}{
      height: 100vh;
     }
     ${Title}{
      font-size: ${TEXT_SIZE.size_normal};
      margin-bottom:0.5em;
      margin-top:0.5em;
     }
     ${Label}{
      font-size: ${TEXT_SIZE.size_normal_mobile};
     }
     ${ItemTitle}{
      font-size: ${TEXT_SIZE.size_normal_mobile};
     }
     ${Footer}{
      min-height: 5vh;
     }
     ${DateText}{
      font-size: ${TEXT_SIZE.size_medium};
     }
     ${ButtonSubmit}{
      -webkit-border-radius: 0.5em;
      -moz-border-radius: 0.5em;
      border-radius: 0.5em;
      padding: 0.7em;
     }
     ${ErrorTitle}{
      width: 75vw;
     }
  
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
    ${Form}{
      width: 80vw;
      column-gap: 0.1em;
      padding: 2.5em;
     }
     ${Header}{
      min-height: 5vh;
     }
     ${Content}{
      height: 90vh;
     }
     ${Title}{
      font-size: ${TEXT_SIZE.size_xlarge_tablet};
      margin-bottom:0.5em;
     }
     ${Label}{
      font-size: ${TEXT_SIZE.size_medium};
     }
     ${ItemTitle}{
      font-size: ${TEXT_SIZE.size_medium};
     }
     ${Footer}{
      min-height: 5vh;
     }
     ${ErrorTitle}{
      width: 35vw;
     }
  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1184px) {
    ${Form}{
      width: 75vw;
      column-gap: 0.1em;
      padding: 2.5em;
     }
  }

  @media screen and (min-width: 1184px) and (max-width: 1344px) {
    ${Form}{
      width: 65vw;
      column-gap: 0.1em;
      padding: 2.5em;
     }
  }

  @media screen and (min-width: 1344px) and (max-width: 1660px) {
    ${Form}{
      width: 55vw;
      column-gap: 0.1em;
      padding: 2.5em;
     }
  }

  @media screen and (min-width: 1660px) and (max-width: 1760px) {
    ${Form}{
      width: 45vw;
      column-gap: 0.1em;
      padding: 2.5em;
     }
  }
`;
