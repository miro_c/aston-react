import styled, {css, createGlobalStyle} from 'styled-components';
import { COLOR, TEXT_SIZE } from '../constants/constant';

export const SelectWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 0.25em;
`;

export const styleCommon = css`
  font-size: ${TEXT_SIZE.size_normal};
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  margin-right: 0.5em;
`;

export const SelectMain = styled.select`
  outline: none;
  background-color: ${COLOR.color_android_green};
  padding: 0.15em;
  margin: 0;
  width: 3.5em;
  font-size: ${TEXT_SIZE.size_xlarge};
  font-family: "Roboto", sans-serif;
  font-weight: 300;
  cursor: inherit;
  line-height: inherit;
  cursor: pointer;
  color: ${COLOR.color_white};
  border: 0.1em solid ${COLOR.color_android_green};
  box-sizing: border-box;
  -webkit-border-radius: 0.35em;
  -moz-border-radius: 0.35em;
  border-radius: 0.35em;

  option {
    color: ${COLOR.color_android_green_dark};
    background: ${COLOR.color_white};
    font-size: ${TEXT_SIZE.size_normal};
    font-family: "Roboto", sans-serif;
    font-weight: 400;
  }

  ${styleCommon}
`;

export const Label = styled.label`
  ${styleCommon}
`;

export const Option = styled.option`
  ${styleCommon}
  background: ${COLOR.color_white};
  color: ${COLOR.color_android_green};
`;

export const SelectValue = styled.p`
  font-size: ${TEXT_SIZE.size_normal};
  color: ${COLOR.color_light_blue};
  font-weight: 900;
`;

/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const SelectResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
       ${SelectValue}{
        font-size: ${TEXT_SIZE.size_medium};
       }
  
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
  
  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1920px) {
  
  }
`;
