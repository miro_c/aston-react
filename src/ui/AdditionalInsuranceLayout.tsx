import React, { useState, FC } from "react";
import { ItemWrapper, Label, Item, InfoWrapper, InfoIcon, InfoTextTitle, InfoText } from "../styles/PageMainStyle";
import Checkbox from "../ui/Checkbox";
import icInsurancePlus from "../assets/ic_insurance_plus.svg";
import { ActionEnum } from "../redux/reducer";

interface Props {
  updateAdditionalInsuranceState: (actionEnum: ActionEnum, isActive: boolean) => void;
  additionalInsuranceActualPrice: number;
}

const AdditionalInsuranceLayout: FC<Props> = ({ updateAdditionalInsuranceState, additionalInsuranceActualPrice }: Props) => {
  const [isCheckedCancelTrip, setCheckedCancelTrip] = useState<boolean>(false);
  const [isCheckedSportActivity, setCheckedSportActivity] = useState<boolean>(false);

  /** Checkbox CancelTrip change event **/
  const handleCheckboxCancelTrip = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const isChecked = event.target.checked;

    /** Set CancelTrip to local state **/
    setCheckedCancelTrip(isChecked);

    /** Update global state **/
    updateAdditionalInsuranceState(ActionEnum.ADDITIONAL_INSURANCE_CANCEL, isChecked)
  };

  /** Checkbox SportActivity change event **/
  const handleCheckboxSportActivity = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const isChecked = event.target.checked;

    /** Set SportActivity to local state **/
    setCheckedSportActivity(isChecked);

    /** Update global state **/
    updateAdditionalInsuranceState(ActionEnum.ADDITIONAL_INSURANCE_ACTIVITY, isChecked)
  };

  return (
    <>
      <ItemWrapper isCenter={false}>
        <Label>Pripoistenia:</Label>
        <Item>
          <Checkbox
            handleChange={handleCheckboxCancelTrip}
            isChecked={isCheckedCancelTrip}
            label="storno cesty"
          />
          <Checkbox
            handleChange={handleCheckboxSportActivity}
            isChecked={isCheckedSportActivity}
            label="športové aktivity"
          />
        </Item>
        <InfoWrapper>
          <InfoIcon src={icInsurancePlus} alt="icFace" />
          <InfoTextTitle>|</InfoTextTitle>
          <InfoText isActive={true}>+
             {additionalInsuranceActualPrice > 0
              ? ' ' + additionalInsuranceActualPrice.toString()
              : " 0"}
            %
          </InfoText>
        </InfoWrapper>
      </ItemWrapper>
    </>
  );
};
export default AdditionalInsuranceLayout;
