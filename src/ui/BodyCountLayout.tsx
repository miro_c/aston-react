import React, { useState, FC } from "react";
import {
  ItemWrapper,
  ErrorTitle,
  Label,
  Item,
  InfoWrapper,
  InfoIcon,
  InfoText,
  InfoTextTitle,
} from "../styles/PageMainStyle";
import SelectPicker from "./SelectPicker";
import { ActionEnum } from "../redux/reducer";
import icFace from "../assets/ic_face.svg";

interface Props {
  updateBodyCountState: (actionEnum: ActionEnum, countNumber: number) => void;
  errorState: boolean;
}

const BodyCountLayout: FC<Props> = ({ updateBodyCountState, errorState }: Props) => {
  const [isCountPicked, setCountPicked] = useState<boolean>(false);
  const [bodyCount, setBodyCount] = useState<number>(1);

  /** Body count select change event **/
  const handleSelectBodyCount = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    const count = event.target.value;
    const countNumber = Number(count);

    /** Set Error to local state **/
    setCountPicked(countNumber > 0)

    /** Set Body count to local state **/
    setBodyCount(countNumber);

    /** Update global state **/
    updateBodyCountState(ActionEnum.BODY_COUNT, countNumber)
  };

  return (
    <>
      <ItemWrapper isCenter={false}>
        {/* Error layout */}
        {errorState && !isCountPicked && (
          <ErrorTitle>Nezvolili ste počet osôb</ErrorTitle>
        )}

        <Label>Počet osôb:</Label>
        <Item>
          <SelectPicker
            countParamValue={bodyCount}
            handleSelectBodyCount={handleSelectBodyCount}
          />
        </Item>
        <InfoWrapper>
          <InfoIcon src={icFace} alt="icFace" />
          <InfoTextTitle>|</InfoTextTitle>
          <InfoText isActive={true}>{bodyCount}</InfoText>
        </InfoWrapper>
      </ItemWrapper>
    </>
  );
};
export default BodyCountLayout;
