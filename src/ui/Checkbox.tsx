import React from "react";
import { CheckboxLabel, CheckboxTitle, CheckboxResponsiveStyle } from "../styles/CheckboxStyle";

interface Props {
  isChecked: boolean;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  label: string;
}

const Checkbox = (props: Props) => {
  return (
    <>
    <CheckboxResponsiveStyle />
      <CheckboxLabel className="container">
        <CheckboxTitle>{props.label}</CheckboxTitle>
        <input
          type="checkbox"
          checked={props.isChecked}
          onChange={props.handleChange}
        />
        <span className="checkmark"></span>
      </CheckboxLabel>
    </>
  );
};
export default Checkbox;
