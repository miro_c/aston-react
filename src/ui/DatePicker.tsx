import React, { FC } from "react";
import icCalendar from "../assets/ic_calendar.svg";
import {
  PickerWrapper,
  PickerInput,
  CalendarText,
  CalendarIcon,
  DatePickerResponsiveStyle
} from "../styles/DatePickerStyle";

interface DatePickerProps {
  handleSelectDateChange: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
  text?: string;
}

const DatePicker: FC<DatePickerProps> = ({
  handleSelectDateChange,
  text,
}: DatePickerProps) => {
  return (
    <>
    <DatePickerResponsiveStyle />
      <PickerWrapper>
        <PickerInput
          type="date"
          id="start"
          name="trip-start"
          value="2022-07-22"
          min="2022-01-01"
          max="2022-12-31"
          onChange={(event) => handleSelectDateChange(event)}
        />
        <CalendarText>{text}</CalendarText>
        <CalendarIcon src={icCalendar} alt="calendar" />
      </PickerWrapper>
    </>
  );
};
export default DatePicker;
