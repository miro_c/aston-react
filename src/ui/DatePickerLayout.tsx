import React, { useState, useEffect, FC } from "react";
import { ItemWrapper, ErrorTitle, DateText, DateTextSpan, Item } from "../styles/PageMainStyle";
import DatePicker from "./DatePicker";
import { getCurrentDate, getFormatedDate } from "../utils/dateUtil";
import { ActionEnum } from "../redux/reducer";
import { InsuranceOptionEnum } from "../model/insurance";
import { getDaysDiff } from "../utils/dateUtil";

interface Props {
  updateDatePickerState: (actionEnum: ActionEnum, dateString: string) => void;
  errorDateStart: boolean;
  errorDateEnd: boolean;
  insuranceOption?: InsuranceOptionEnum;
}

const DatePickerLayout: FC<Props> = ({ updateDatePickerState, errorDateStart, errorDateEnd, insuranceOption }: Props) => {
  const [isDateStartPicked, setIsDateStartPicked] = useState<boolean>(false);
  const [isDateEndPicked, setIsDateEndPicked] = useState<boolean>(false);
  const [dateDiffError, setDateDiffError] = useState<boolean>(false);
  const [dateStartString, setDateStartString] = useState<string>('');
  const [dateEndString, setDateEndString] = useState<string>('');

  const [dateStart, setDateStart] = useState<string>(() => {
    return getCurrentDate();
  });
  const [dateEnd, setDateEnd] = useState<string>(() => {
    return getCurrentDate();
  });
  const [dateStartIsActive, setDateStartIsActive] = useState<boolean>();
  const [dateEndIsActive, setDateEndIsActive] = useState<boolean>();

  /** Check if Date End is set correctly **/
  useEffect(() => {
    checkDayDiff(dateStartString, dateEndString);

  }, [dateStartString, dateEndString]);

  /** DateStart change event **/
  const handleSelectDateStartChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const dateStartString = event.target.value;
    setDateStartString(dateStartString)

    /** Set local error state **/
    setIsDateStartPicked(true);

    /** Format date for Ui **/
    const formatedDate = getFormatedDate(dateStartString);
    setDateStart(formatedDate);
    setDateStartIsActive(true);

    /** Update global state **/
    updateDatePickerState(ActionEnum.SELECT_DATE_START, dateStartString);
  };

  /** DateEnd change event **/
  const handleSelectDateEndChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const dateEndString = event.target.value;
    setDateEndString(dateEndString)

    /** Set local error state **/
    setIsDateEndPicked(true);

    /** Format date for Ui **/
    const formatedDate = getFormatedDate(dateEndString);
    setDateEnd(formatedDate);
    setDateEndIsActive(true);

    /** Update global state **/
    updateDatePickerState(ActionEnum.SELECT_DATE_END, dateEndString);
  };

  /** Check if DateEnd is not set lower as DateStart  **/
  const checkDayDiff = (dayStart: string, dayEnd: string) => {
    if (dayStart && dayEnd) {
      const diff = getDaysDiff(dayEnd, dayStart);
      setDateDiffError(diff < 1);
    }
  };

  return (
    <>
      <ItemWrapper isCenter={true}>
        {/* Error layout */}
        {errorDateStart && !isDateStartPicked && (
          <ErrorTitle>Zabudli ste zvoliť začiatok poistenia</ErrorTitle>
        )}

        {errorDateEnd &&
          !isDateEndPicked &&
          insuranceOption === InsuranceOptionEnum.INSURANCE_SHORT && (
            <ErrorTitle>Zabudli ste zvoliť koniec poistenia</ErrorTitle>
          )}

        {dateDiffError && (
          <ErrorTitle>
            Koniec poistenia musí byť neskôr ako začiatok poistenia
          </ErrorTitle>
        )}

        <Item>
          <DatePicker
            handleSelectDateChange={handleSelectDateStartChange}
            text={"Začiatok poistenia"}
          />
          <DateText isActive={dateStartIsActive!}>{dateStart}</DateText>
          <DateTextSpan isActive={dateEndIsActive!}>-</DateTextSpan>
          <DateText
            isActive={
              insuranceOption !== InsuranceOptionEnum.INSURANCE_YEAR &&
              dateEndIsActive!
            }
          >
            {dateEnd}
          </DateText>
          <DatePicker
            handleSelectDateChange={handleSelectDateEndChange}
            text={"Koniec poistenia"}
          />
        </Item>
      </ItemWrapper>
    </>
  );
};
export default DatePickerLayout;
