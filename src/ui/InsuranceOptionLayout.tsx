import React, { useState, FC } from "react";
import {
  ItemWrapper,
  ErrorTitle,
  Label,
  Item,
  InfoWrapper,
  InfoIcon,
  InfoTextTitle,
  InfoText,
  ItemTitle,
  RadioButtonLabel,
  RadioButton,
  AppResponsiveStyle
} from "../styles/PageMainStyle";
import { FORM } from "../constants/constant";
import { ActionEnum } from "../redux/reducer";
import icCalendarShort from "../assets/ic_calendar_short.svg";
import icCalendarYear from "../assets/ic_calendar_year.svg";

interface Props {
  updateInsuranceOptionState: (actionEnum: ActionEnum) => void;
  dayCount: number;
  errorState: boolean;
}

const InsuranceOptionLayout: FC<Props> = ({ updateInsuranceOptionState, dayCount, errorState }: Props) => {
  const [isChecked, setIsChecked] = useState<boolean>(false);
  const [insuranceOptionSelect, setInsuranceOptionSelect] = useState<string>("");

  /** Insurance option change event **/
  const handleSelectInsuranceOption = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = event.target.value;
    switch (value) {
      case FORM.OPTION_INSURANCE_SHORT:

        /** Set local Error state **/
        setIsChecked(true)

        /** Set local state **/
        setInsuranceOptionSelect(value);

        /** Update Global state **/
        updateInsuranceOptionState(ActionEnum.OPTION_INSURANCE_SHORT)
        break;

      case FORM.OPTION_INSURANCE_YEAR:
        setIsChecked(true)
        setInsuranceOptionSelect(value);

        /** Update Global state **/
        updateInsuranceOptionState(ActionEnum.OPTION_INSURANCE_YEAR)
        break;

      default:
        break;
    }
  };

  const getDayCountText = (count: number): string => {
    let dayText = "";
    switch (count) {
      case 1:
        dayText = "deň";
        break;

      case 2:
      case 3:
      case 4:
        dayText = "dni";
        break;

      default:
        dayText = "dní";
        break;
    }

    return dayText;
  };

  return (
    <>
      <AppResponsiveStyle />
      <ItemWrapper isCenter={false}>
        {/* Error layout */}
        {errorState && !isChecked && (
          <ErrorTitle>Chýba variant poistenia</ErrorTitle>
        )}

        <Label>Variant poistenia:</Label>
        <Item>
          <RadioButton
            type="radio"
            name="radio_1"
            value={FORM.OPTION_INSURANCE_SHORT}
            checked={insuranceOptionSelect === FORM.OPTION_INSURANCE_SHORT}
            onChange={(event) => handleSelectInsuranceOption(event)}
          />
          <RadioButtonLabel />
          <ItemTitle>Krátkodobé poistenie</ItemTitle>
        </Item>
        <Item>
          <RadioButton
            type="radio"
            name="radio_2"
            value={FORM.OPTION_INSURANCE_YEAR}
            checked={insuranceOptionSelect === FORM.OPTION_INSURANCE_YEAR}
            onChange={(event) => handleSelectInsuranceOption(event)}
          />
          <RadioButtonLabel />
          <ItemTitle>Celoročné poistenie</ItemTitle>
        </Item>
        <InfoWrapper>
          {insuranceOptionSelect === FORM.OPTION_INSURANCE_YEAR && (
            <>
              <InfoIcon src={icCalendarYear} alt="icCalendar" />
              <InfoTextTitle>|</InfoTextTitle>
              <InfoText isActive={true}>Rok</InfoText>
            </>
          )}
          {insuranceOptionSelect === FORM.OPTION_INSURANCE_SHORT && (
            <>
              <InfoIcon src={icCalendarShort} alt="icCalendar" />
              <InfoTextTitle>|</InfoTextTitle>
              <InfoText isActive={true}>{dayCount < 0? 0 : dayCount} {getDayCountText(dayCount)}</InfoText>
            </>
          )}
        </InfoWrapper>
      </ItemWrapper>
    </>
  );
};
export default InsuranceOptionLayout;
