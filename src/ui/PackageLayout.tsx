import React, { useState, FC } from "react";
import {
  ItemWrapper,
  ErrorTitle,
  Label,
  Item,
  InfoWrapper,
  InfoIcon,
  InfoTextTitle,
  InfoText,
  ItemTitle,
  RadioButtonLabel,
  RadioButton,
} from "../styles/PageMainStyle";
import { FORM } from "../constants/constant";
import { ActionEnum } from "../redux/reducer";
import icBox from "../assets/ic_box.svg";

interface Props {
  updatePackageState: (actionEnum: ActionEnum) => void;
  packageActualPrice: number;
  errorState: boolean;
}

const PackageLayout: FC<Props> = ({ updatePackageState, packageActualPrice, errorState }: Props) => {
  const [packageSelect, setPackageSelect] = useState<string>("");
  const [isRadioButtonChecked, setRadioButtonChecked] = useState<boolean>(false);

  /** Package option change event **/
  const handleSelectPackage = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    
    /** Set error local state **/
    setRadioButtonChecked(true)

    switch (value) {
      case FORM.OPTION_PACKAGE_BASE:

        /** Set local state **/
        setPackageSelect(value);

        /** Update global state **/
        updatePackageState(ActionEnum.OPTION_PACKAGE_BASE)
        break;

      case FORM.OPTION_PACKAGE_EXTENDED:
        setPackageSelect(value);
        updatePackageState(ActionEnum.OPTION_PACKAGE_EXTENDED)
        break;

      case FORM.OPTION_PACKAGE_EXTRA:
        setPackageSelect(value);
        updatePackageState(ActionEnum.OPTION_PACKAGE_EXTRA)
        break;

      default:
        break;
    }
  };

  return (
    <>
      <ItemWrapper isCenter={false}>
        {/* Error layout */}
        {errorState && !isRadioButtonChecked && <ErrorTitle>Je nutné vybrať Balík</ErrorTitle>}

        <Label>Balík:</Label>
        <Item>
          <RadioButton
            type="radio"
            name="radio_3"
            value={FORM.OPTION_PACKAGE_BASE}
            checked={packageSelect === FORM.OPTION_PACKAGE_BASE}
            onChange={(event) => handleSelectPackage(event)}
          />
          <RadioButtonLabel />
          <ItemTitle>základný</ItemTitle>
        </Item>
        <Item>
          <RadioButton
            type="radio"
            name="radio_4"
            value={FORM.OPTION_PACKAGE_EXTENDED}
            checked={packageSelect === FORM.OPTION_PACKAGE_EXTENDED}
            onChange={(event) => handleSelectPackage(event)}
          />
          <RadioButtonLabel />
          <ItemTitle>rozšírený</ItemTitle>
        </Item>
        <Item>
          <RadioButton
            type="radio"
            name="radio_5"
            value={FORM.OPTION_PACKAGE_EXTRA}
            checked={packageSelect === FORM.OPTION_PACKAGE_EXTRA}
            onChange={(event) => handleSelectPackage(event)}
          />
          <RadioButtonLabel />
          <ItemTitle>extra</ItemTitle>
        </Item>
        <InfoWrapper>
          <InfoIcon src={icBox} alt="icFace" />
          <InfoTextTitle>|</InfoTextTitle>
          <InfoText isActive={true}>{packageActualPrice > 0? packageActualPrice.toFixed(2) : '0.00'} &euro;</InfoText>
        </InfoWrapper>
      </ItemWrapper>
    </>
  );
};
export default PackageLayout;
