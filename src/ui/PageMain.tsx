import { useState, useEffect, useCallback, FC } from "react";
import {
  MainGrid,
  Header,
  Content,
  Footer,
  Form,
  Title,
  ButtonWrapper,
  ButtonSubmit,
  FinalPriceWrapper,
  FinalPriceText,
  FinalPriceTitle,
  AppResponsiveStyle,
  ContinuousPriceWrapper,
  ContinuousText
} from "../styles/PageMainStyle";
import {State} from '../redux/state';
import { CalculatorState } from '../redux/reducer';
import { useSelector, useDispatch } from 'react-redux';
import { getDaysDiff } from "../utils/dateUtil";
import { getInsuranceFinalPrice, getPackageCurrentPrice, getAdditionalInsuranceCurrentPrice } from "../utils/insuranceUtil";
import { InsuranceOptionEnum, InsurancePackageEnum, Insurance } from "../model/insurance";
import InsuranceOptionLayout  from "./InsuranceOptionLayout";
import DatePickerLayout  from "./DatePickerLayout";
import PackageLayout  from "./PackageLayout";
import AdditionalInsuranceLayout  from "./AdditionalInsuranceLayout";
import BodyCountLayout  from "./BodyCountLayout";
import { ActionEnum } from "../redux/reducer";
import {
  dispatchAction,
  dispatchActionDateStart,
  dispatchActionDateEnd,
  dispatchActionCancelTrip,
  dispatchActionSportActivity,
  dispatchActionBodyCount
} from "../redux/dispatcher";

const PageMain: FC = () => {
  const [dayCount, setDayCount] = useState<number>(0);
  const [packageCurrentPrice, setPackageCurrentPrice] = useState<number>(0);
  const [additionalInsuranceCurrentPrice, setAdditionalInsuranceCurrentPrice] = useState<number>(0);
  const [insuranceOptionError, setInsuranceOptionError] = useState<boolean>(false);
  const [dateStartError, setDateStartError] = useState<boolean>(false);
  const [dateEndError, setDateEndError] = useState<boolean>(false);
  const [insurancePackageError, setInsurancePackageError] = useState<boolean>(false);
  const [bodyConuntError, setBodyConuntError] = useState<boolean>(false);
  const [continuousInsurancePrice, setContinuousInsurancePrice] = useState<number>(0);
  const [finalInsurancePrice, setFinalInsurancePrice] = useState<number>(0);
 
  /** Get Dispatcher from redux **/
  const dispatch = useDispatch();

  /** Get current global state from redux **/
  const calculatorState = useSelector((state: State) => state.calculatorState) as CalculatorState;
  const currentInsuranceObject = calculatorState.data;

  /** Get actual day count number **/
  useEffect(() => {
    setDayCount(
      getDaysDiff(
        currentInsuranceObject?.insuranceDateEnd!,
        currentInsuranceObject?.insuranceDateStart!
      )
    );
  }, [
    currentInsuranceObject?.insuranceDateEnd,
    currentInsuranceObject?.insuranceDateStart,
  ]);

  /** Get current price for insurance Package **/
  useEffect(() => {
    if (currentInsuranceObject?.insurancePackage !== InsurancePackageEnum.PACKAGE_DEFAULT) {
      setPackageCurrentPrice(getPackageCurrentPrice(currentInsuranceObject, dayCount));
    }

  }, [currentInsuranceObject, packageCurrentPrice, dayCount]);

  /** Get price for Additional insurance **/
  useEffect(() => {
    if (
      currentInsuranceObject?.insurancePackage !==
      InsurancePackageEnum.PACKAGE_DEFAULT
    ) {
      setAdditionalInsuranceCurrentPrice(
        getAdditionalInsuranceCurrentPrice(currentInsuranceObject)
      );
    }
  }, [currentInsuranceObject, additionalInsuranceCurrentPrice]);

  /** Get continuous insurance price **/
  useEffect(() => {
    (async () => {
      const finalPrice = await getInsuranceFinalPrice(currentInsuranceObject!);
      setContinuousInsurancePrice(finalPrice > 0? finalPrice : 0);
    })();
  }, [currentInsuranceObject]);

  /** Button calculate final price **/
  const actionButtonSubmit = async (): Promise<void> => {
    const currentState = currentInsuranceObject;
    if (currentState != null) {
      /** Check for form error **/
      if (await handleFormErrorSate(currentState)) {
        const finalPrice = await getInsuranceFinalPrice(currentState);
        setFinalInsurancePrice(finalPrice > 0? finalPrice : 0);
      }
    }
  };

  /** Set Error state for form fields **/
  const handleFormErrorSate = async (insuranceObject: Insurance): Promise<boolean> => {

    /** SendErrorState to InsuranceOptionLayout **/
    setInsuranceOptionError(
      insuranceObject?.insuranceOption === InsuranceOptionEnum.INSURANCE_DEFAULT
    );

    /** SendErrorState to DatePickerLayout **/
    setDateStartError(insuranceObject?.insuranceDateStart == null);
    if (insuranceObject?.insuranceOption === InsuranceOptionEnum.INSURANCE_SHORT) {
      setDateEndError(insuranceObject?.insuranceDateEnd == null);
    } 

    /** SendErrorState to Package RadioButton layout **/
    setInsurancePackageError(
      insuranceObject?.insurancePackage === InsurancePackageEnum.PACKAGE_DEFAULT
    );

    /** SendErrorState to BodyCount layout **/
    setBodyConuntError(insuranceObject?.bodyCount === 0);

    if (insuranceObject?.insuranceOption === InsuranceOptionEnum.INSURANCE_SHORT) {
      setDateEndError(insuranceObject?.insuranceDateEnd == null);
    } 

    return checkErrorState(insuranceObject)
  };

  /** Check final Error state for form **/
  const checkErrorState = async (insuranceObject: Insurance): Promise<boolean> => {
    let insuranceDateEndOk = true;
    if (insuranceObject?.insuranceOption === InsuranceOptionEnum.INSURANCE_SHORT) {
      insuranceDateEndOk = insuranceObject?.insuranceDateEnd != null;
    }

    const dayDiffNumber = getDaysDiff(
      insuranceObject?.insuranceDateEnd!,
      insuranceObject?.insuranceDateStart!
    )
    const isDatesError = dayDiffNumber < 1;

    return (
      (insuranceObject?.insuranceOption !== InsuranceOptionEnum.INSURANCE_DEFAULT &&
      insuranceObject?.insuranceDateStart != null) &&
      insuranceDateEndOk &&
      (insuranceObject?.insurancePackage !== InsurancePackageEnum.PACKAGE_DEFAULT) &&
      insuranceObject?.bodyCount !== 0 &&
      !(isDatesError && insuranceObject?.insuranceOption === InsuranceOptionEnum.INSURANCE_SHORT)
    );
  };

  /** Dispatch update Insurance option to reducer **/
  const dispatchInsuranceOptionState = useCallback((actionEnum: ActionEnum) => {
      dispatchAction(dispatch, actionEnum);
    },[dispatch]
  );
  
  /** Dispatch update Date Start/End to reducer **/
  const dispatchDatePickerState = useCallback(
    (actionEnum: ActionEnum, dateString: string) => {
      switch (actionEnum) {
        case ActionEnum.SELECT_DATE_START:
          dispatchActionDateStart(dispatch, actionEnum, dateString);
          break;

        case ActionEnum.SELECT_DATE_END:
          dispatchActionDateEnd(dispatch, actionEnum, dateString);
          break;

        default:
          break;
      }
    },
    [dispatch]
  );

  /** Dispatch update Package to reducer **/
  const dispatchPackageState = useCallback((actionEnum: ActionEnum) => {
      dispatchAction(dispatch, actionEnum);
    },[dispatch]
  );

  /** Dispatch update Additional Insurance to reducer **/
  const dispatchAdditionalInsuranceState = useCallback((actionEnum: ActionEnum, isActive: boolean) => {
    switch (actionEnum) {
      case ActionEnum.ADDITIONAL_INSURANCE_CANCEL:
        dispatchActionCancelTrip(dispatch, actionEnum, isActive);
        break;

      case ActionEnum.ADDITIONAL_INSURANCE_ACTIVITY:
        dispatchActionSportActivity(dispatch, actionEnum, isActive);
        break;

      default:
        break;
    }
  },[dispatch])

  /** Dispatch update Body Count to reducer **/
  const dispatchBodyCountState = useCallback((actionEnum: ActionEnum, countNumber: number) => {
      dispatchActionBodyCount(dispatch, actionEnum, countNumber);
    }, [dispatch]
  );

  return (
    <>
      <AppResponsiveStyle />
      <MainGrid>
        <Header></Header>
        <Content>
          <Form>
            <Title>Kalkulačka cestovného poistenia</Title>

            {/* Insurance option layout */}
            <InsuranceOptionLayout
              updateInsuranceOptionState={dispatchInsuranceOptionState}
              dayCount={dayCount}
              errorState={insuranceOptionError}
            />

            {/* Date Picker layout */}
            <DatePickerLayout
              updateDatePickerState={dispatchDatePickerState}
              errorDateStart={dateStartError}
              errorDateEnd={dateEndError}
              insuranceOption={currentInsuranceObject?.insuranceOption}
            />

            {/* Package RadioButton layout */}
            <PackageLayout
              updatePackageState={dispatchPackageState}
              packageActualPrice={packageCurrentPrice}
              errorState={insurancePackageError}
            />

            {/* Additional Insurance CheckBox layout */}
            <AdditionalInsuranceLayout
              updateAdditionalInsuranceState={dispatchAdditionalInsuranceState}
              additionalInsuranceActualPrice={additionalInsuranceCurrentPrice}
            />

            {/* BodyCount layout */}
            <BodyCountLayout
              updateBodyCountState={dispatchBodyCountState}
              errorState={bodyConuntError}
            />

            {/* Continuous price layout */}
            <ContinuousPriceWrapper>
              <ContinuousText>Priebežná cena <br/>{continuousInsurancePrice} &euro;</ContinuousText>
            </ContinuousPriceWrapper>

            {/* Final Insurance price layout */}
            <FinalPriceWrapper>
              <FinalPriceText></FinalPriceText>
              <FinalPriceTitle>{finalInsurancePrice} &euro;</FinalPriceTitle>
            </FinalPriceWrapper>

            {/* Button calculation Insurance layout */}
            <ButtonWrapper>
              <ButtonSubmit onClick={(): Promise<void> => actionButtonSubmit()}>
                Prepočet poistenia
              </ButtonSubmit>
            </ButtonWrapper>
          </Form>
        </Content>
        <Footer />
      </MainGrid>
    </>
  );
};;

export default PageMain;
