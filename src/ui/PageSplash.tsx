import { FC, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import icChart from "../assets/ic_addchart.svg";
import { PageSplashMain, SplashLogo } from "../styles/PageSplashStyle";

const PageSplash: FC = () => {
  const navigate = useNavigate();

  useEffect(() => {
    goToNextPage();
  });

  const goToNextPage = () => {
    setTimeout(() => {
      navigate("/form", { replace: true });
    }, 2000);
  };

  return (
    <>
      <PageSplashMain>
        <SplashLogo src={icChart}/>
      </PageSplashMain>
    </>
  );
};

export default PageSplash;