import { FC } from "react";
import {
  SelectWrapper,
  SelectValue,
  Label,
  SelectMain,
  Option,
  SelectResponsiveStyle
} from "../styles/SelectPickerStyle";

interface SelectPickerProps {
  countParamValue?: number;
  handleSelectBodyCount?: (event: React.ChangeEvent<HTMLSelectElement>) => void;
}

const SelectPicker: FC<SelectPickerProps> = ({
  countParamValue,
  handleSelectBodyCount,
}: SelectPickerProps) => {
  return (
    <>
    <SelectResponsiveStyle />
      <SelectWrapper>
        <Label>
          <SelectMain value={countParamValue} onChange={handleSelectBodyCount}>
            <Option value={1}>1</Option>
            <Option value={2}>2</Option>
            <Option value={3}>3</Option>
          </SelectMain>
        </Label>
        <SelectValue>(1 až 3)</SelectValue>
      </SelectWrapper>
    </>
  );
};

export default SelectPicker;
