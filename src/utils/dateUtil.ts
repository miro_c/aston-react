
/** Get current Date **/
export const getCurrentDate = (): string => {
  const dateCurrent = new Date();
  const date = `${dateCurrent.getDate()}.${dateCurrent.getMonth() + 1}.${dateCurrent.getFullYear()}`;
  return date;
};

export const getFormatedDate = (dateString: string): string => {
    const dateCurrent = new Date(dateString);
    const date = `${dateCurrent.getDate()}.${dateCurrent.getMonth() + 1}.${dateCurrent.getFullYear()}`;
    return date;
};

export const getDaysDiff = (dateFrom?: string, dateTo?: string): number => {
    const from = new Date(dateFrom!);
    const to = new Date(dateTo!);
    const oneDay = 1000*60*60*24;
    return  Math.ceil((from!.getTime() - to!.getTime())/(oneDay))
}
