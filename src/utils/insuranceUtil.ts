import { InsuranceOptionEnum, InsurancePackageEnum, Insurance, InsuranceAdditional } from "../model/insurance";
import { getDaysDiff } from "../utils/dateUtil";
import { TARIF } from "../constants/constant";

export const getInsuranceFinalPrice = async (insuranceObject: Insurance): Promise<number> => {
    let priceFinal = 0;
    
    /**  Insurance option (short / year) **/
    switch (insuranceObject?.insuranceOption) {
      case InsuranceOptionEnum.INSURANCE_SHORT:
        priceFinal = await computeForInsuranceShort(
          insuranceObject?.insuranceDateStart!,
          insuranceObject?.insuranceDateEnd!,
          insuranceObject?.insurancePackage!,
          insuranceObject?.additionalInsurance?.isCancelTtrip,
          insuranceObject?.additionalInsurance?.isSportActivity,
          insuranceObject?.bodyCount!
        );
        break;

      case InsuranceOptionEnum.INSURANCE_YEAR:
        priceFinal = await computeForInsuranceYear(
          insuranceObject?.insurancePackage!,
          insuranceObject?.additionalInsurance?.isCancelTtrip,
          insuranceObject?.additionalInsurance?.isSportActivity,
          insuranceObject?.bodyCount!
        );
        break;

      default:
        break;
    }

    return priceFinal
};

/** Get insurance price for Short time **/
const computeForInsuranceShort = async (
  dateStart?: string,
  dateEnd?: string,
  insurancePackage?: InsurancePackageEnum,
  isCancelTtrip?: boolean,
  isSportActivity?: boolean,
  bodyCount?: number
): Promise<number> => {

  /**  Number of days: **/
  const diffDays = getDaysDiff(dateEnd, dateStart);

  let tarif = 1
  let extraTarifCancelTrip = 1
  let extraTarifSportAktivity = 1

  /**  Package: **/
  tarif = getPackageInsuranceTarifShort(insurancePackage)

  /**  Extra cancel trip: **/
  if(isCancelTtrip){
    extraTarifCancelTrip = TARIF.EXTRA.T_50 // +50%
  }

  /**  Extra sports activity: **/
  if(isSportActivity){
    extraTarifSportAktivity = TARIF.EXTRA.T_30  // +30%
  }

  /** Final result for Short time insurance **/
  const finalResult = diffDays * tarif * extraTarifCancelTrip * extraTarifSportAktivity * bodyCount!
  return Number(finalResult.toFixed(2));

};

/** Get insurance price for Year **/
const computeForInsuranceYear = async (
  insurancePackage?: InsurancePackageEnum,
  isCancelTtrip?: boolean,
  isSportActivity?: boolean,
  bodyCount?: number
): Promise<number> => {

  let tarif = 1;
  let extraTarifCancelTrip = 1;
  let extraTarifSportAktivity = 1;

  /**  Package: **/
  tarif = getPackageInsuranceTarifYear(insurancePackage!)

  /**  Extra cancel trip: **/
  if (isCancelTtrip) {
    extraTarifCancelTrip = TARIF.EXTRA.T_20; // +50%
  }

  /**  Extra sports activity: **/
  if (isSportActivity) {
    extraTarifSportAktivity = TARIF.EXTRA.T_10; // +30%
  }

  /** Final result for Year insurance **/
  const finalResult = tarif * extraTarifCancelTrip * extraTarifSportAktivity * bodyCount!
  return Number(finalResult.toFixed(2));
  
};

export const getPackageCurrentPrice = (
  currentInsuranceObject?: Insurance,
  dayCount?: number
): number => {
  let tarif = 1;
  let dayCountNumber = 1;
  switch (currentInsuranceObject?.insuranceOption) {
    case InsuranceOptionEnum.INSURANCE_SHORT:
      dayCountNumber = dayCount! > 0? dayCount! : 0;
      tarif = getPackageInsuranceTarifShort(
        currentInsuranceObject?.insurancePackage
      );
      break;

    case InsuranceOptionEnum.INSURANCE_YEAR:
      tarif = getPackageInsuranceTarifYear(
        currentInsuranceObject?.insurancePackage
      );
      break;
  }

  const price = tarif * dayCountNumber!;
  return price;
};

export const getAdditionalInsuranceCurrentPrice = (
  currentInsuranceObject?: Insurance
): number => {
  let tarif = 0;
  switch (currentInsuranceObject?.insuranceOption) {
    case InsuranceOptionEnum.INSURANCE_SHORT:
      tarif = getAdditionalInsuranceTarifShort(currentInsuranceObject?.additionalInsurance);
      break;

    case InsuranceOptionEnum.INSURANCE_YEAR:
      tarif = getAdditionalInsuranceTarifYear(currentInsuranceObject?.additionalInsurance);

      break;
  }

  return tarif;
};

/*****************************************************************************
 *                    private helper methods
 * ***************************************************************************/
const getPackageInsuranceTarifShort = (insurancePackage?: InsurancePackageEnum) => {
  let tarif = 0;

  switch (insurancePackage) {
    case InsurancePackageEnum.PACKAGE_BASE:
      tarif = TARIF.SHORT.T_12; // 1.2,- eur/den
      break;

    case InsurancePackageEnum.PACKAGE_EXTENDED:
      tarif = TARIF.SHORT.T_18; // 1.8,- eur/den
      break;

    case InsurancePackageEnum.PACKAGE_EXTRA:
      tarif = TARIF.SHORT.T_24; // 2.4,- eur/den
      break;

    default:
      break;
  }

  return tarif;
};

const getPackageInsuranceTarifYear = (insurancePackage?: InsurancePackageEnum) => {
  let tarif = 0;

  switch (insurancePackage) {
    case InsurancePackageEnum.PACKAGE_BASE:
      tarif = TARIF.YEAR.T_39; // 39,- eur/rok
      break;

    case InsurancePackageEnum.PACKAGE_EXTENDED:
      tarif = TARIF.YEAR.T_49; // 49,- eur/rok
      break;

    case InsurancePackageEnum.PACKAGE_EXTRA:
      tarif = TARIF.YEAR.T_59; // 59,- eur/rok
      break;

    default:
      break;
  }

  return tarif;
};

const getAdditionalInsuranceTarifShort = (additionalInsurance?: InsuranceAdditional) => {
  let tarif = 0;
  let tarifPercent = 0;

  if (additionalInsurance?.isCancelTtrip) {
    tarif = TARIF.EXTRA.T_50; // 50%
    tarifPercent += 50;
  }

  if (additionalInsurance?.isSportActivity) {
    tarif = TARIF.EXTRA.T_30; // 30%
    tarifPercent += 30;
  }

  return tarifPercent;
};

const getAdditionalInsuranceTarifYear = (additionalInsurance?: InsuranceAdditional) => {
  let tarif = 0;
  let tarifPercent = 0;

  if (additionalInsurance?.isCancelTtrip) {
    tarif += TARIF.EXTRA.T_20; // 20%
    tarifPercent += 20
  }

  if (additionalInsurance?.isSportActivity) {
    tarif += TARIF.EXTRA.T_10; // 10%
    tarifPercent += 10
  }

  return tarifPercent;
};

